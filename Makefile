APPLICATION_NAME = aplicacionweb
LOCAL_ADDRESS = 127.0.0.1
LOCAL_PORT = 8080
CONTAINER_PORT = 8080
CONTAINER_MOUNT = C:\ProgramacionAppWeb\aplicacionweb\webapps

docker-image:
	docker image rm -f tomcat:9
	docker build --tag tomcat:9 .

docker-container:
	
	docker container rm \
		-f $(APPLICATION_NAME) 
	docker container create \
		--name $(APPLICATION_NAME) \
		--tty --interactive \
		--publish $(LOCAL_ADDRESS):$(LOCAL_PORT):$(CONTAINER_PORT) \
		--mount type=bind,source="C:\ProgramacionAppWeb\aplicacionweb\webapps",target=/usr/local/tomcat/webapps \
		tomcat:9

docker-start:
	docker container start \
		--attach --interactive $(APPLICATION_NAME)

docker-stop:
	docker container stop \
		$(APPLICATION_NAME)

docker-shell:
	docker container exec \
		--tty --interactive \
		$(APPLICATION_NAME) \
		bash

docker-containers:
	docker container ls \
		-a

mvn-clean:
	mvn clean

mvn-compile:
	mvn compile

mvn-package:
	mvn package

mvn-test:
	mvn test

mvn-versions:
	mvn -U versions:use-latest-releases versions:update-properties

mvn-dependencies:
	mvn dependency:resolve

mvn-validate:
	mvn validate

mvn-verify:
	mvn verify

mvn-paArriba: mvn-validate mvn-verify mvn-deploy

mvn-deploy: mvn-clean mvn-package
	XCopy C:\ProgramacionAppWeb\aplicacionweb\target\ROOT.war C:\ProgramacionAppWeb\aplicacionweb\webapps


heroku-deploy:
	mvn clean heroku:deploy-war