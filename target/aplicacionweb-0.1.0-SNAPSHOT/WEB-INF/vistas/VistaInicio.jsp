<%--
  -- Directiva **page**, establece lo siguiente:
  --
  -- *  language: lenguaje de programación a usar en este JSP: java
  -- *  contenType: tipo de contenido que generará este JSP: text/html
  -- *  pageEncoding: codificación de carácteres a usar para este JSP: UTF-8


--%>



<%@ page
    language="java"
    contentType="text/html;charset=utf-8"
    pageEncoding="utf-8"
%>



<%--
  -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!

  --
  -- JSP forza el inicio-creación de una sesión (JSESSIONID) con el
  -- cliente (navegador web) por lo que todo cliente creará una sesión.
  --
  -- Si se quiere desactivar esta carácteristica se establece el
  -- atributo **session** a **false** en la directiva **page** para todos
  -- los JSP que uno use en el proyecto.
  

  -- <%@ page
  --    session="false"
  --    language="java"
  --    ...
  -- %>
  --
  --
  --
  -- Si se asigna false al atributo session la variable global session
  -- (usada más abajo) deja de estar disponible en el JSP.
  --
  -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
  -- ¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡¡ IMPORTANTE !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
--%>

<%--
  -- Importación de clases propias de Java a este JSP, así como de clases
  -- creadas por uno mismo.
--%>
<%@ page
    import="java.util.List"
    import="java.util.Map"
    import="java.util.ArrayList"

    import="programacionweb.utilerias.UrlRelativo"
%>


<%--
  -- Un JSP tiene acceso a varios **objetos globales** u **objetos implicitos**:
  --
  -- *  HttpServletRequest     request
  -- *  HttpServletResponse    response
  -- *  HttpSession            session
  -- *  ServletContext         application
  -- *  ServletConfig          config
--%>

<%--
  -- Declaración y asignación, así como acceso a los datos (atributos)
  -- enviados-compartidos por el Controlador haciendo uso del objeto *request*.
  --
  --
  --
  -- El objeto *request* es la misma instancia obtenida por el Controlador en
  -- el parámetro "HttpServletRequest solicitud" del método doGet().
--%>

<%--
  -- Declaración e inicialización de otras **variables globales**
--%>
<%
  String htmlTitle = "VistaInicio.jsp";
  String mainCss = UrlRelativo.css(request, "/main.css");
%>

<!DOCTYPE html>
<html lang="en">
<head>
  <title>Aplicacion Web</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
  <link rel="stylesheet" href="<%= mainCss %>">
</head>
<body>
  <form action="" method="POST">
    <div class="container">
      <div class="row">
        <div class="col-md-12">
          <div class="well well-sm" style="background-color: #9ECFC5;">
              <form class="form-horizontal" method="post">
                  <fieldset>
                      <legend class="text-center header">Cifrado y Decifrado Opcion 1</legend>
                      <div class="form-group">
                          <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                          <div class="col-md-8" style="padding-bottom: 10px;">
                            <label for="state_id" class="control-label">Ingrese el texto</label>
                              <textarea class="form-control" id="message" placeholder="Ingrese el texto a cifrar." rows="7" name = "EntradaTexto"></textarea>
                          </div>
                      </div>
                      <div class="form-group">
                        <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                        <div class="col-md-8" style="padding-bottom: 10px;">
                          <label for="state_id" class="control-label">Ingrese la clave</label>
                            <textarea class="form-control" id="message" placeholder="Ingrese la clave a cifrar." rows="1" name = "EntradaTextoPermu"></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                      <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                      <div class="col-md-8" style="padding-bottom: 10px;">
                        <label for="state_id" class="control-label">Resultado</label>
                          <textarea class="form-control" id="message" placeholder="Esperando resultado..." rows="5"  id="resultado" name="resultado">${resultado}</textarea>
                      </div>
                      </div>
                      <div class="form-group">
                      <span class="col-md-1 col-md-offset-2 text-center"><i class="fa fa-pencil-square-o bigicon"></i></span>
                      <div class="col-md-8" style="padding-bottom: 10px;">
                      <button class="btn btn-primary btn-lg" type="submit" id="boton" name="accion" value="boton" style="margin: 1em 7em;">Cifrar</button>
                      <button class="btn btn-primary btn-lg" type="submit" id="boton2" name="accion" value="boton2" style="margin: 1em 7em;">Decifrar</button>
                      <button type="submit" class="btn btn-primary btn-lg" name="accion"value="Atbash" style="margin: 1em 7em;">Cifrado en Atbash</button>
                      </div>
                    </div>
                  </fieldset>
              </form>
          </div>
      </div>
  </div>
</div>
  </form>
</body>
</html>
