package programacionweb.Modelos;

public class modelo {    
    public modelo(){}
    public static boolean verificaPermutacion(String permutacion){
        boolean validacion = true;
        if(permutacion==null){
        //Verifica que cada numero desde 1 hasta el valor del periodo (longitud de la permutacion)
        //esten dentro de la permutacion sin importar el orden
        for(int i=1; i<=permutacion.length(); i++){            
            if(permutacion.indexOf((char)(i +'0')) == -1)
                validacion = false;
        }
        }else{
            validacion = false;
        }
        return validacion;
    }
    public static String cifrado(String texto, String permutacion){
        int periodo = permutacion.length();        
        String mensajeCifrado = "";
        
        //Relleno de faltantes
        int faltantes = texto.length() % periodo;
        for(int i=faltantes; i<=periodo; i++)
            texto += " ";
        //Cifrado
        for(int i=0; i<texto.length() - periodo; i+=periodo){
            //Crear grupos del tamaño del periodo
            char[] grupo = texto.substring(i, i + periodo).toCharArray();
         
            //Almacena el nuevo grupo cifrado
            char[] grupoNuevo = new char[periodo];            
            for(int j=0; j<periodo; j++){ 
                
                //Convertir a numero cada carater de la permutacion
                int index = Character.getNumericValue(permutacion.charAt(j) - 1);
                
                grupoNuevo[j] = grupo[index];
            }
            mensajeCifrado += String.valueOf(grupoNuevo);
        } 
        
        return mensajeCifrado;
    }
    public static boolean tdesi(String textoCifrado, String permutacion) {
    	int periodo = permutacion.length();
    	boolean vale= false;
    	int faltantes = textoCifrado.length() % periodo;
    	if (faltantes ==0) {
    		vale=true;
    	}
    	return vale;
    }
    public static String descifrado(String textoCifrado, String permutacion){
        int periodo = permutacion.length();        
        String mensajeDescifrado = "";
                
        for(int i=0; i<textoCifrado.length(); i+=periodo){
            //Crear grupos del tamaño del periodo
            char[] grupo = textoCifrado.substring(i, i + periodo).toCharArray();
            
            char[] grupoNuevo = new char[periodo];
            for(int j=0; j<periodo; j++){
                
                //Convertir a numero cada carater de la permutacion
                int index = Character.getNumericValue(permutacion.charAt(j) - 1);
                
                grupoNuevo[index] = grupo[j];
            }  
            
            mensajeDescifrado += String.valueOf(grupoNuevo);
        }    
        return mensajeDescifrado;        
    }    
   
}
