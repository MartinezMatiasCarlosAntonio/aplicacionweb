package programacionweb.controladores;

import programacionweb.utilerias.Suma;
import java.io.PrintWriter;
import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import programacionweb.Modelos.modelo;
import programacionweb.Modelos.Atbash;

@WebServlet("/inicio")
public class ControladorInicio extends HttpServlet 
{
  public  String men,per,alfabeto;
  protected void doGet(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException{
       
        rep(solicitud, respuesta);
  }
  protected void doPost(HttpServletRequest solicitud, HttpServletResponse respuesta)
  throws ServletException, IOException{
   String resultado="";
    if(solicitud.getParameter("accion")!=null)
    {
      
            
      men=solicitud.getParameter("EntradaTexto");
      per=solicitud.getParameter("EntradaTextoPermu");
      alfabeto=solicitud.getParameter("Entradadealfabeto");

      
      if(solicitud.getParameter("accion").equalsIgnoreCase("boton2A"))
      {
        resultado=Atbash.codificar(men,alfabeto);
        solicitud.setAttribute("resultado", resultado);
      rep2(solicitud, respuesta);
      }
      else if(solicitud.getParameter("accion").equalsIgnoreCase("botonA"))
      {
        resultado=Atbash.codificar(men,alfabeto);
        solicitud.setAttribute("resultado", resultado);
      rep2(solicitud, respuesta);
      }else


      if(solicitud.getParameter("accion").equalsIgnoreCase("grupos"))
      {
        rep(solicitud, respuesta);
      }else

      
      if(solicitud.getParameter("accion").equalsIgnoreCase("Atbash"))
      {
        rep2(solicitud, respuesta);
      }else      
      if(solicitud.getParameter("accion").equalsIgnoreCase("boton"))
      {
        resultado=modelo.cifrado(men, per);
        solicitud.setAttribute("resultado", resultado);
      rep(solicitud, respuesta);
      }
      else if(solicitud.getParameter("accion").equalsIgnoreCase("boton2")){
        resultado=modelo.descifrado(men, per);
                solicitud.setAttribute("resultado", resultado);
      rep(solicitud, respuesta);
      }
        
      
    }
    
}

  protected void rep(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaInicio.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }

      protected void rep2(HttpServletRequest solicitud, HttpServletResponse respuesta)
      throws ServletException, IOException
      {
        var contextoServlet = solicitud.getServletContext();
        var despachadorSolicitud =
        contextoServlet.getRequestDispatcher("/WEB-INF/vistas/VistaNew.jsp");
        despachadorSolicitud.forward(solicitud, respuesta);
      }

      
}
